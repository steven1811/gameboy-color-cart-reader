/*
 * GBCReader.c
 *
 * Created: 26.12.2015 10:34:15
 * Author : Steven
 */ 

//Configuration
#include "Main.h"

//Menu Strings
	const PROGMEM uint8_t _SMainMenu[]="Main Menu:";
	const PROGMEM uint8_t _SReadROM[]="1: Read ROM";
	const PROGMEM uint8_t _SReadHeader[]="2: Read Header";
	const PROGMEM uint8_t _SBackMainMenu[]="1: Return to Main Menu";
	const PROGMEM uint8_t _SRefreshHeader[]="2: Refresh Header";	
	const PROGMEM uint8_t _SHeaderMenu[]="Header Menu:";
	const PROGMEM uint8_t _SReadRAM[]="3: Read RAM";
	const PROGMEM uint8_t _SWriteRAM[]="4: Write RAM";	

//General Info
	const PROGMEM uint8_t _STitle[]="Full title: ";
	const PROGMEM uint8_t _SControllerType[]="Memory Banking Controller: ";
	const PROGMEM uint8_t _SROMSize[]="ROM Size: ";
	const PROGMEM uint8_t _SVerifyLogo[]="Nintendo Logo: ";
	const PROGMEM uint8_t _SCGBFlag[]="Gameboy Color Flag: ";
	const PROGMEM uint8_t _SSGBFlag[]="Super Gameboy Flag: ";
	const PROGMEM uint8_t _SOldLicenseeCode[]="Old Licensee Code: ";
	const PROGMEM uint8_t _SNewLicenseeCode[]="New Licensee Code: ";				
	const PROGMEM uint8_t _SManufacturerCode[]="Manufacturer Code: ";
	const PROGMEM uint8_t _SMaskROMNumber[]="Mask ROM Number: ";
	const PROGMEM uint8_t _SDestinationCode[]="Destination Code: ";
	const PROGMEM uint8_t _SHeaderChecksum[]="Header Checksum: ";
	const PROGMEM uint8_t _SGlobalChecksum[]="Global Checksum: ";

//Features:
	const PROGMEM uint8_t _SFeatures[]="Features: ";
	const PROGMEM uint8_t _SRAM[]="RAM";
	const PROGMEM uint8_t _SRTC[]="RTC";
	const PROGMEM uint8_t _SBattery[]="Battery";
		
//Controller types
	const PROGMEM uint8_t _SNone[]="None";
	const PROGMEM uint8_t _SMBC1[]="MBC1";
	const PROGMEM uint8_t _SMBC2[]="MBC2";	 
	const PROGMEM uint8_t _SMBC3[]="MBC3";
	const PROGMEM uint8_t _SMBC4[]="MBC4";
	const PROGMEM uint8_t _SMBC5[]="MBC5";	

//Various Stuff
	const PROGMEM uint8_t _SReadContinue[]="Before Continuing you should clear the screen, enter a 1, after finishing save it to a file and press 1 to continue";
	const PROGMEM uint8_t _SLogoMatch[]="Logo matches! (OK)";
	const PROGMEM uint8_t _SLogoNoMatch[]="Logo doesn't match! (ERROR?)";
		
//Flag Strings
	const PROGMEM uint8_t _SSupportCGBOldAlso[]="This game supports Gameboy Color Functions, but also works on older gameboys";
	const PROGMEM uint8_t _SSupportCGBOnly[]="This game only supports Gameboy Color Functions. Doesn't work on older gameboys";
	const PROGMEM uint8_t _SSupportsNoSGB[]="This Game doesn't use any Super Gameboy Functions.";
	const PROGMEM uint8_t _SSuportsSGB[]="This game utilizes special Super Gameboy Functions";
	
//Sizes RAM
	const PROGMEM uint8_t _SNoneRAM[]="None";
	const PROGMEM uint8_t _S2KRAM[]="2KB";
	const PROGMEM uint8_t _S8KRAM[]="8KB";
	const PROGMEM uint8_t _S32KRAM[]="32KB (4 banks)";
	const PROGMEM uint8_t _S128KRAM[]="128KB (16 banks)";
	const PROGMEM uint8_t _S64KRAM[]="64KB (8 banks)";
	
//Sizes ROM
	const PROGMEM uint8_t _S32KROM[]="32KB";
	const PROGMEM uint8_t _S64KROM[]="64KB (4 banks)";	
	const PROGMEM uint8_t _S128KROM[]="128KB (8 banks)";
	const PROGMEM uint8_t _S256KROM[]="256KB (16 banks)";
	const PROGMEM uint8_t _S512KROM[]="512KB (32 banks)";
	const PROGMEM uint8_t _S1MROM[]="1MB (64 banks)";
	const PROGMEM uint8_t _S2MROM[]="2MB (128 banks)";
	const PROGMEM uint8_t _S4MROM[]="4MB (256 banks)";
	const PROGMEM uint8_t _S1_1MROM[]="1.1MB (72 banks)";
	const PROGMEM uint8_t _S1_2MROM[]="1.2MB (80 banks)";
	const PROGMEM uint8_t _S1_5MROM[]="1.5MB (96 banks)";	
	
//Destination Codes
	const PROGMEM uint8_t _SDestinationJAP[]="Japanese";
	const PROGMEM uint8_t _SDestinationNonJAP[]="Non-Japanese";
	
//Various
	const PROGMEM uint8_t _SUnknown[]="(Unknown)";
	const PROGMEM uint8_t _SChecksumMatches[]="(Checksum matches!) (OK)";
	const PROGMEM uint8_t _SChecksumNoMatch[]="(Checksum doesn't match!) (ERROR)";	
	
	CartHeader *header;

uint8_t waitforRequiredChar(uint8_t start, uint8_t end) {
	uint8_t letter=0;
	
	while(1) {
		letter=readChar();
		if(letter>=start && letter<=end) {
			break;
		}
	}
	
	return letter;
	
}

void showMainMenu() {
	writeChar('\n');
	writeLine_P(_SMainMenu);
	writeLine_P(_SReadROM);
	writeLine_P(_SReadHeader);
	writeLine_P(_SReadRAM);	
	writeLine_P(_SWriteRAM);	
	uint8_t letter;
	letter=waitforRequiredChar('1','4');

	switch(letter) {
		case '1':
			writeLine_P(_SReadContinue);
			letter=waitforRequiredChar('1','1');
			if(letter=='1') {
				readROM(writeChar);
				letter=waitforRequiredChar('1','1');
				if(letter=='1') {
					showMainMenu();
				}
			}
			break;
		case '2':
			readGameCartHeader();
			header=getCartHeader();
			showHeaderMenu();
			break;
		case '3':
			writeLine_P(_SReadContinue);
			letter=waitforRequiredChar('1','1');
			if(letter=='1') {
				readRAM(writeChar);
				letter=waitforRequiredChar('1','1');
				if(letter=='1') {
					showMainMenu();
				}
			}
			break;
		case '4':
			writeLine_P(_SReadContinue);
			letter=waitforRequiredChar('1','1');
			if(letter=='1') {
				writeRAM(readChar);
				letter=waitforRequiredChar('1','1');
				if(letter=='1') {
					showMainMenu();
				}
			}			
		default:
			break;
	}
}

void printIntToString(uint8_t *location, uint8_t size) {
	uint8_t i=0;
	uint8_t buffer[64];
	
	for(i=0;i!=size;i++) {
		writeString_P(PSTR("0x"));
		writeString(utoa(location[i], buffer, 16));
		writeChar(',');
	}
}

void wVerifyLogo() {
	writeString_P(_SVerifyLogo);
	if(verifyLogo()==0) {
		writeLine_P(_SLogoMatch);
	}
	else {
		writeLine_P(_SLogoNoMatch);
	}	
}

void wTitle() {
	writeString_P(_STitle);
	writeBytes(header->title,titleSize);
	writeChar('\n');
}

void wManufacturerCode() {
	writeString_P(_SManufacturerCode);
	printIntToString(header->manufacturerCode, manufacturerCodeSize);
	writeChar('\n');
}

void wCGBFlag(){
	writeString_P(_SCGBFlag);
	switch(header->cgbFlag) {
		case cgbFlagCGBOnly:
			writeLine_P(_SSupportCGBOnly);
			break;
		case cgbFlagOldGameboy:
			writeLine_P(_SSupportCGBOldAlso);
			break;
		default:
			printIntToString(&header->cgbFlag, 1);
			writeLine_P(_SUnknown);		
			break;
	}	
}

void wNewLicenseeCode() {
	writeString_P(_SNewLicenseeCode);
	writeBytes(header->newLicenseeCode, newLicenseeCodeSize);
	writeChar('\n');
}

void wSGBFlag() {
	writeString_P(_SSGBFlag);
	switch(header->sgbFlag) {
		case sgbFlagSGBSupport:
			writeLine_P(_SSuportsSGB);
			break;
		case sgbFlagNoSGB:
			writeLine_P(_SSupportsNoSGB);
			break;
		default:
			printIntToString(&header->sgbFlag, 1);
			writeLine_P(_SUnknown);
			break;
	}	
}


//Add more types
void wCartridgeType() {
	writeString_P(_SControllerType);
	switch(header->usesController) {
		case 0:
			writeLine_P(_SNone);
			break;			
		case 1:
			writeLine_P(_SMBC1);
			break;			
		case 2:
			writeLine_P(_SMBC2);
			break;	
		case 3:
			writeLine_P(_SMBC3);
			break;			
		case 4:
			writeLine_P(_SMBC4);
			break;			
		case 5:
			writeLine_P(_SMBC5);
			break;	
	}
}

void wROMSize() {
	writeString_P(_SROMSize);
	switch(header->romSize) {
		case romSize32K:
			writeLine_P(_S32KROM);
			break;
		case romSize64K:
			writeLine_P(_S64KROM);
			break;
		case romSize128K:
			writeLine_P(_S128KROM);
			break;
		case romSize256K:
			writeLine_P(_S256KROM);
			break;
		case romSize512K:
			writeLine_P(_S512KROM);
			break;
		case romSize1M:
			writeLine_P(_S1MROM);
			break;
		case romSize2M:
			writeLine_P(_S2MROM);
			break;
		case romSize4M:
			writeLine_P(_S4MROM);
			break;
		case romSize1_1M:
			writeLine_P(_S1_1MROM);
			break;
		case romSize1_2M:
			writeLine_P(_S1_2MROM);
			break;
		case romSize1_5M:
			writeLine_P(_S1_5MROM);
			break;
		default:
			printIntToString(&header->romSize, 1);
			writeLine_P(_SUnknown);
			break;
	}	
}

void wDestinationCode() {
	writeString_P(_SDestinationCode);
	switch(header->destination) {
		case destinationJapanese:
			writeLine_P(_SDestinationJAP);
			break;		
		case destinationNonJapanese:
			writeLine_P(_SDestinationNonJAP);
			break;
		default:
			printIntToString(&header->destination, 1);
			writeLine_P(_SUnknown);
			break;		
	}
}

void wOldLicenseeCode() {
	writeString_P(_SOldLicenseeCode);
	printIntToString(&header->oldLicenseeCode, 1);
	writeChar('\n');	
}

void wMaskROMNumber() {
	writeString_P(_SMaskROMNumber);
	printIntToString(&header->maskRomNumber, 1);
	writeChar('\n');
}

void wHeaderChecksum() {	
	writeString_P(_SHeaderChecksum);
	printIntToString(&header->headerChecksum, 1);	
	
	uint8_t temp=checkHeaderChecksum();
	if(temp==0) {
		writeLine_P(_SChecksumMatches);
	}
	else {
		printIntToString(&temp, 1);
		writeLine_P(_SChecksumNoMatch);		
	}
}

void wGlobalChecksum() {
	writeString_P(_SGlobalChecksum);
	printIntToString(header->globalChecksum, globalChecksumSize);
	writeChar('\n');	
}

void wFeatures() {
	writeString_P(_SFeatures);

	if(header->hasRAM==true) {
		writeString_P(_SRAM);
		writeChar(' ');
		switch(header->ramSize) {
			case ramSizeNone:
				writeString_P(_SNoneRAM);
				break;
			case ramSize2K:
				writeString_P(_S2KRAM);
				break;
			case ramSize8K:
				writeString_P(_S8KRAM);
				break;
			case ramSize32K:
				writeString_P(_S32KRAM);
				break;
			case ramSize64K:
				writeString_P(_S64KRAM);
				break;
			case ramSize128K:
				writeString_P(_S128KRAM);
				break;
			default:
				printIntToString(&header->ramSize, 1);
				writeString_P(_SUnknown);
				break;
		}
		writeChar(',');
	}
	
	if(header->hasBattery==true) {
		writeString_P(_SBattery);
		writeChar(',');
	}
	
	if(header->hasRTC==true) {
		writeString_P(_SRTC);
	}
	writeChar('\n');
}

void showHeaderMenu() {
	writeChar('\n');
	writeLine_P(_SHeaderMenu);
	wTitle();
	wROMSize();		
	wVerifyLogo();
	wCGBFlag();
	wSGBFlag();
	wOldLicenseeCode();
	wNewLicenseeCode();		
	wManufacturerCode();
	wMaskROMNumber();
	wDestinationCode();
	wHeaderChecksum();
	wGlobalChecksum();
	wCartridgeType();
	wFeatures();
	writeLine_P(_SBackMainMenu);
	writeLine_P(_SRefreshHeader);
	
	uint8_t letter=0;
	letter=waitforRequiredChar('1','2');
	switch(letter) {
		case '1':
			showMainMenu();
			break;
		case '2':
			readGameCartHeader();
			break;
	}
	showHeaderMenu();
}

int main()
{
	uart_init();
	initROM();
	showMainMenu();
}