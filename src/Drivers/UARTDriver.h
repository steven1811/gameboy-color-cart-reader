/*
 * UARTDriver.h
 *
 * Created: 28.12.2015 16:06:21
 *  Author: Steven
 */ 


#ifndef UARTDRIVER_H_
	#define UARTDRIVER_H_
	#include "../Configuration.h"
	#include <avr/io.h>
	#include <util/setbaud.h>
	#include <string.h>
	#include <avr/pgmspace.h>
	#include <stdarg.h>
	
	//Function Prototypes
		void writeChar(const uint8_t letter);
		void writeString(const uint8_t *string);
		void writeBytes(uint8_t *location, uint8_t size);
		void uart_init();
		uint8_t readChar();
		void writeString_P(const uint8_t *string);
		void writeStrings_P(const uint8_t *seperator ,const uint8_t *strings, ...);
#endif /* UARTDRIVER_H_ */