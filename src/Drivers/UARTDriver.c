/*
 * UARTDriver.c
 *
 * Created: 28.12.2015 15:41:08
 *  Author: Steven
 */ 
#include "UARTDriver.h"

void writeChar(const uint8_t letter) {
	while (!(UCSRA & (1 << UDRE)));
	UDR = letter;
}

void writeString(const uint8_t *string) {
	uint16_t i;

	for(i=0;i!=strlen(string);i++) {
		writeChar(string[i]);
	}
}

void writeString_P(const uint8_t *string) {
	while (pgm_read_byte(string)!=0x00 ) {
		writeChar(pgm_read_byte(string++));
	}
}

void writeStrings_P(const uint8_t *seperator ,const uint8_t *strings, ...) {
	uint8_t *string;
	va_list arguments;
	va_start(arguments, strings);
	string=strings;

	
	do{
		writeString_P(string);
		string=va_arg ( arguments, uint8_t*);	
		if(string!=0) {
			writeString_P(seperator);				
		}
	}while(string!=0);
	
	va_end(arguments);
}

void writeLine_P(const uint8_t *string) {
	while (pgm_read_byte(string)!=0x00 ) {
		writeChar(pgm_read_byte(string++));
	}
	writeChar('\n');
}

void writeBytes(uint8_t *location, uint8_t size) {
	uint8_t i=0;
	
	for(i=0;i!=size;i++)  {
		writeChar(location[i]);
	}
}

void uart_init()
{
	UBRRH = UBRRH_VALUE;
	UBRRL = UBRRL_VALUE;
	
	#if USE_2X
	UCSRA |= (1 << U2X);
	#else
	UCSRA &= ~(1 << U2X);
	#endif
	
	UCSRB = (1<<RXEN) | (1<<TXEN);
	UCSRC = (1 << URSEL)|(1<<UCSZ1) | (1<<UCSZ0);
}

uint8_t readChar() {
	while (!(UCSRA & (1<<RXC)));
	return UDR;
}
