/*
 * ROMReader.h
 *
 * Created: 05.01.2016 22:01:34
 *  Author: Steven
 */ 


#ifndef ROMREADER_H_
#define ROMREADER_H_
#include "GBReader.h"

void setROMBank(uint8_t bank);
uint8_t getROMBankCounter();
void readROMBank(uint8_t bankNr, void (*function) (uint8_t));
void readROMBanks(uint8_t bankStart, uint8_t bankEnd, void (*function) (uint8_t));
void readROM(void (*function) (uint8_t));



#endif /* ROMREADER_H_ */