/*
 * RAMReader.h
 *
 * Created: 05.01.2016 22:09:29
 *  Author: Steven
 */ 


#ifndef RAMREADER_H_
	#define RAMREADER_H_
	#include "GBReader.h"

	void setRAMEnabled(bool state);
	void setRAMBank(uint8_t bank);
	void readRAMBank(uint8_t bankNr, void (*function) (uint8_t));
	void readRAMBanks(uint8_t bankStart, uint8_t bankEnd, void (*function) (uint8_t));
	void readRAM(void (*function) (uint8_t));
	void writeRAM(uint8_t (*function) (void));
#endif /* RAMREADER_H_ */