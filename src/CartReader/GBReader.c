/*
 * GBReader.c
 *
 * Created: 28.12.2015 15:41:38
 *  Author: Steven
 */ 

#include "GBReader.h"

void initROM() {
	A0ToA7DDR = 0xFF;
	A0ToA7Port = 0x00;
	
	A8ToA15DDR = 0xFF;
	A8ToA15Port = 0x00;
	
	DataDDR= 0x00;
	DataPort = 0x00;
	
	CTRLDDR = 0xFF;
	CTRLPort = (1<<READ) | (1<<WRITE) | (1<<RESET) | (1<<CS);

	//Pulse Reset
	_delay_ms(100);
	CTRLPort&= ~(1<<RESET);
	_delay_ms(100);
	CTRLPort|=(1<<RESET);

	//writeAdress(0)
	writeAddress(0x00);
	
	enableClock();
}

void  enableClock() {
	TCCR1B|=(1<<WGM12) | (1<<CS10) /*| (1<<CS12)*/;
	TCCR1A|=(1<<COM1B0);	
	TCNT1|=0;	
	OCR1A|=0x0007;
}

//Done
void readGameCartHeader() {
	gameCartHead.logo=readDataBytesToRAM(logoAddress, logoSize);
	gameCartHead.title=readDataBytesToRAM(titleAddress, titleSize);
	gameCartHead.manufacturerCode=readDataBytesToRAM(manufacturerCodeAddress, manufacturerCodeSize);
	gameCartHead.cgbFlag=readDataByte(cgbFlagAddress);
	gameCartHead.newLicenseeCode=readDataBytesToRAM(newLicenseeCodeAddress, newLicenseeCodeSize);
	gameCartHead.sgbFlag=readDataByte(sgbFlagAddress);

	//Setup Cart Type
	uint8_t temp=readDataByte(cartTypeAddress);
	
	switch(temp) {
		case cartTypeROMOnly:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=ROM;
			gameCartHead.hasRAM=false;
			gameCartHead.hasBattery=false;
			gameCartHead.hasRTC=false;				
			break;
		case cartTypeROMRAM:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=ROM;
			gameCartHead.hasRAM=true;
			gameCartHead.hasBattery=false;		
			gameCartHead.hasRTC=false;					
			break;
		case cartTypeROMRAMBattery:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=ROM;
			gameCartHead.hasRAM=true;
			gameCartHead.hasBattery=true;
			gameCartHead.hasRTC=false;			
			break;		
		
		case cartTypeMBC1:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC1;
			gameCartHead.hasBattery=false;
			gameCartHead.hasRAM=false;
			gameCartHead.hasRTC=false;		
			break;
		case cartTypeMBC1RAM:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC1;
			gameCartHead.hasRAM=true;
			gameCartHead.hasBattery=false;		
			gameCartHead.hasRTC=false;		
			break;
		case cartTypeMBC1RAMBattery:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC1;
			gameCartHead.hasRAM=true;
			gameCartHead.hasBattery=true;	
			gameCartHead.hasRTC=false;						
			break;
			
		//MBC2 has integrated RAM available
		case cartTypeMBC2:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC2;
			gameCartHead.hasRAM=true;		
			gameCartHead.hasBattery=false;	
			gameCartHead.hasRTC=false;				
			break;
		case cartTypeMBC2Battery:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC2;
			gameCartHead.hasRAM=true;
			gameCartHead.hasBattery=true;	
			gameCartHead.hasRTC=false;						
			break;
		
		case cartTypeMBC3:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC3;		
			gameCartHead.hasBattery=false;
			gameCartHead.hasRAM=false;
			gameCartHead.hasRTC=false;				
			break;		
		case cartTypeMBC3RAM:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC3;
			gameCartHead.hasBattery=false;			
			gameCartHead.hasRAM=true;	
			gameCartHead.hasRTC=false;				
			break;
		case cartTypeMBC3RAMBattery:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC3;
			gameCartHead.hasRAM=true;
			gameCartHead.hasBattery=true;
			gameCartHead.hasRTC=false;			
			break;					
		case cartTypeMBC3TimerBattery:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC3;
			gameCartHead.hasBattery=true;
			gameCartHead.hasRTC=true;	
			gameCartHead.hasRAM=false;				
			break;
		case cartTypeMBC3TimerRAMBattery:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC3;
			gameCartHead.hasBattery=true;
			gameCartHead.hasRTC=true;	
			gameCartHead.hasRAM=true;			
			break;

		case cartTypeMBC4:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC4;	
			gameCartHead.hasRTC=false;		
			gameCartHead.hasRAM=false;			
			gameCartHead.hasBattery=false;						
			break;
		case cartTypeMBC4RAM:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC4;
			gameCartHead.hasRAM=true;	
			gameCartHead.hasBattery=false;
			gameCartHead.hasRTC=false;								
			break;
		case cartTypeMBC4RAMBattery:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC4;
			gameCartHead.hasRAM=true;
			gameCartHead.hasBattery=true;	
			gameCartHead.hasRTC=false;						
			break;
		case cartTypeMBC5:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC5;	
			gameCartHead.hasRTC=false;	
			gameCartHead.hasRAM=false;
			gameCartHead.hasBattery=false;							
			break;
		case cartTypeMBC5RAM:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC5;
			gameCartHead.hasRAM=true;
			gameCartHead.hasBattery=false;	
			gameCartHead.hasRTC=false;												
			break;
		case cartTypeMBC5RAMBattery:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC5;
			gameCartHead.hasRAM=true;
			gameCartHead.hasBattery=true;
			gameCartHead.hasRTC=false;						
			break;			
		case cartTypeMBC5Rumble:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC5;	
			gameCartHead.hasRTC=false;
			gameCartHead.hasRAM=false;
			gameCartHead.hasBattery=false;									
			break;
		case cartTypeMBC5RumbleRAM:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC5;
			gameCartHead.hasRAM=true;		
			gameCartHead.hasBattery=false;
			gameCartHead.hasRTC=false;							
			break;
		case cartTypeMBC5RumbleRAMBattery:
			gameCartHead.isUnsupported=false;
			gameCartHead.usesController=MBC5;
			gameCartHead.hasRAM=true;
			gameCartHead.hasBattery=true;	
			gameCartHead.hasRTC=false;						
			break;

		//Unsupported yet
		case cartTypeMMM01:
			gameCartHead.isUnsupported=true;
			gameCartHead.usesController=MMM01;
			gameCartHead.hasRTC=false;
			gameCartHead.hasRAM=false;
			gameCartHead.hasBattery=false;						
			break;
		case cartTypeMMM01RAM:
			gameCartHead.isUnsupported=true;
			gameCartHead.usesController=MMM01;
			gameCartHead.hasRAM=true;
			gameCartHead.hasBattery=false;	
			gameCartHead.hasRTC=false;						
			break;
		case cartTypeMMM01RAMBattery:
			gameCartHead.isUnsupported=true;
			gameCartHead.usesController=MMM01;
			gameCartHead.hasRTC=false;						
			gameCartHead.hasRAM=true;
			gameCartHead.hasBattery=true;
			break;
		case cartTypePocketCamera:
			gameCartHead.isUnsupported=true;
			break;
		case cartTypeBandaiTama5:
			gameCartHead.isUnsupported=true;
			break;
		case cartTypeHuC3:
			gameCartHead.isUnsupported=true;
			break;
		case cartTypeHuC1RamBattery:
			gameCartHead.isUnsupported=true;
			gameCartHead.hasRAM=true;
			gameCartHead.hasBattery=true;			
			break;
		default:
			gameCartHead.isUnsupported=true;
			break;
	}		

	gameCartHead.romSize=readDataByte(romSizeAddress);
	gameCartHead.ramSize=readDataByte(ramSizeAddress);
	gameCartHead.destination=readDataByte(destinationAddress);
	gameCartHead.oldLicenseeCode=readDataByte(oldLicenseeCodeAddress);
	gameCartHead.maskRomNumber=readDataByte(maskRomNumberAddress);
	gameCartHead.headerChecksum=readDataByte(headerChecksumAddress);
	gameCartHead.globalChecksum=readDataBytesToRAM(globalChecksumAddress, globalChecksumSize);
}



//Done
void writeAddress(uint16_t address) {
	A0ToA7Port= address & 0x00FF;
	A8ToA15Port= address >> 8;
	return;
}

//Done
void writeDataByte(uint16_t address, uint8_t data) {
	DataPort=0x00;
	DataDDR= 0xFF;
	
	//CLOCK WAIT HIGH
	while((TIFR & (1<<OCF1A))==0);
	TIFR |= (1<<OCF1A);
		
	//RD HIGH Und Address Change
	writeAddress(address);
	CTRLPort|=(1<<READ);

	//CS LOW
	CTRLPort&=~(1<<READ);
	
	//CLOCK WAIT LOW
	while((TIFR & (1<<OCF1A))==0);
	TIFR |= (1<<OCF1A);
	
	//Write data
	DataPort=data;
	
	//WR LOW
	CTRLPort&=~(1<<WRITE);
	
	//WR HIGH
	CTRLPort|=(1<<WRITE);	
	
	//CLOCK WAIT HIGH
	while((TIFR & (1<<OCF1A))==0);
	TIFR |= (1<<OCF1A);
		
	//CS HIGH
	CTRLPort|=(1<<CS);	
	return;
}

//Done
uint8_t readDataByte(uint16_t address) {
	DataDDR= 0x00;
	DataPort=0x00;	

	uint8_t retval;

	writeAddress(address);
	
	//CS LOW READ LOW
	CTRLPort&=~((1<<CS) | (1<<READ));	

	while((TIFR & (1<<OCF1A))==0);
	TIFR |= (1<<OCF1A);

	retval=DataPin;
	
	while((TIFR & (1<<OCF1A))==0);
	TIFR |= (1<<OCF1A);
	
	//RD High
	CTRLPort|=(1<<CS) | (1<<READ);
	
	return retval;
}

//Done
uint8_t* readDataBytesToRAM(uint16_t startAddress, uint8_t size) {
	uint8_t i=0;
	uint8_t *p = (uint8_t*) malloc((size)*sizeof(uint8_t));
	
	for(i=0;i!=size;i++)  {
		p[i]=readDataByte(startAddress+i);
	}
	return p;
}

void readDataFromTo(uint16_t startAddress, uint16_t endAddress, void (*function) (uint8_t)) {
	uint16_t i=0;
	for(i=startAddress;i<=endAddress;i++) {
		function(readDataByte(i));
	}
}

//Done
CartHeader* getCartHeader() {
	CartHeader *p=&gameCartHead;
	return p;
}

//Done
const uint8_t PROGMEM nintendoLogoBitmap[]= {
	0xCE, 0xED, 0x66, 0x66, 0xCC, 0x0D, 0x00, 0x0B, 0x03, 0x73, 0x00, 0x83, 0x00, 0x0C, 0x00, 0x0D,
	0x00, 0x08, 0x11, 0x1F, 0x88, 0x89, 0x00, 0x0E, 0xDC, 0xCC, 0x6E, 0xE6, 0xDD, 0xDD, 0xD9, 0x99,
	0xBB, 0xBB, 0x67, 0x63, 0x6E, 0x0E, 0xEC, 0xCC, 0xDD, 0xDC, 0x99, 0x9F, 0xBB, 0xB9, 0x33, 0x3E
};

//Done
uint8_t verifyLogo() {
	uint8_t i=0;
	
	for(i=0;i!=logoSize;i++) {
		if(gameCartHead.logo[i]!=pgm_read_byte(&nintendoLogoBitmap[i])) {
			return i;
		}
	}
	return 0;
}

//Done
uint8_t checkHeaderChecksum() {
	uint8_t calcChecksum=0;
	uint16_t i=0;
	for(i=headerChecksumStartAddress;i<=headerChecksumEndAddress;i++) {
		calcChecksum=calcChecksum-readDataByte(i)-1;
	}
	
	if(calcChecksum==gameCartHead.headerChecksum) {
		return 0;
	}
	else {
		return calcChecksum;
	}
	
}
