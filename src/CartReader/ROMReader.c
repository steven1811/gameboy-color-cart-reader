/*
 * ROMReader.c
 *
 * Created: 05.01.2016 22:01:48
 *  Author: Steven
 */ 

#include "ROMReader.h"

void setROMBank(uint8_t bank) {
	switch(gameCartHead.usesController) {
		case MBC1: //MBC1
		writeDataByte(0x6000, 0x00); //Set ROM Mode
		writeDataByte(0x2100, (bank & 0x1F));
		
		if(bank>0x1F) {
			writeDataByte(0x2100, (bank & 0b01100000)>>5);
		}

		break;
		case MBC2:
		writeDataByte(0x0000, 0x00); //Rom Switch
		writeDataByte(0x2100, (bank & 0b00001111));
		break;
		case MBC3:
		writeDataByte(0x2100, bank);
		break;
		case MBC4://Might not work
		writeDataByte(0x2100, bank);
		break;
		case MBC5:
		writeDataByte(0x0000, 0x00); //Disable RAM
		writeDataByte(0x2100, bank);
		break;
	}
}

uint8_t getROMBankCounter() {
	uint8_t romSize=0;
	
	switch(gameCartHead.romSize) {
		case romSize32K:
		romSize= 0;
		break;
		case romSize64K:
		romSize= 3;
		break;
		case romSize128K:
		romSize= 7;
		break;
		case romSize256K:
		romSize= 15;
		break;
		case romSize512K:
		romSize= 31;
		break;
		case romSize1M:
		romSize= 63;
		break;
		case romSize2M:
		romSize= 127;
		break;
		case romSize4M:
		romSize= 255;
		break;
		case romSize1_1M:
		romSize= 71;
		break;
		case romSize1_2M:
		romSize= 79;
		break;
		case romSize1_5M:
		romSize= 95;
		break;
	}
	return romSize;
}

void readROMBank(uint8_t bankNr, void (*function) (uint8_t)) {
	if(bankNr==0) {
		readDataFromTo(0x0000, 0x3FFF, function);
	}
	else{
		setROMBank(bankNr);
		readDataFromTo(0x4000, 0x7FFF, function);
	}
}

void readROMBanks(uint8_t bankStart, uint8_t bankEnd, void (*function) (uint8_t)) {
	uint16_t i=0;
	for(i=bankStart;i<=bankEnd;i++) {
		readROMBank(i, function);
	}
}

void readROM(void (*function) (uint8_t)) {
	readGameCartHeader();
	readROMBank(0x00 ,function);
	uint8_t bankCounter=getROMBankCounter();
	readROMBanks(1,bankCounter,function);
}