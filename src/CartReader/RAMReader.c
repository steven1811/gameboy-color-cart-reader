/*
 * RAMReader.c
 *
 * Created: 05.01.2016 22:09:18
 *  Author: Steven
 */ 

#include "RAMReader.h"


void setRAMBank(uint8_t bank) {
	switch(gameCartHead.usesController) {
		case MBC2:
			break;
		
		default:
			writeDataByte(0x4000, bank);
			break;
	}
}

uint8_t getRAMBankCounter() {
	uint8_t ramSize=0;
	
	switch(gameCartHead.ramSize) {
		//Doesnt work
		case ramSizeNone:
			ramSize= 0;
			break;
			
		//Doesnt work
		case ramSize2K:
			ramSize= 0;
			break;
				
		case ramSize8K:
			ramSize= 0;
			break;
			
		case ramSize32K:
			ramSize= 3;
			break;		
		case ramSize64K:
			ramSize= 7;
			break;
					
		case ramSize128K:
			ramSize= 15;
			break;
	}
	return ramSize;
}

void setRAMEnabled(bool state) {
	uint8_t actual=0;
	
	switch(state) {
		case true:
			actual=0x0A;
			break;
		case false:
			actual=0x00;
			break;
	}
	//MBC2 3 (Enables also Timer) 5
	switch(gameCartHead.usesController) {
		case MBC1:
			if(state==true) {
				writeDataByte(0x6100, 0x01);//Enable RAM Bank Mode
			}
			else {
				writeDataByte(0x6100, 0x00);//Disable RAM Bank Mode and switch to ROM Bank mode
			}
			writeDataByte(0x0000, actual);
			break;
		case MBC2:
			writeDataByte(0x0000, actual);
			break;
		case MBC3:
			writeDataByte(0x0000, actual);
		case MBC4:
			writeDataByte(0x0000, actual);
			break;
		case MBC5:
			writeDataByte(0x0000, actual);
			break;
	}
}

void readRAMBank(uint8_t bankNr, void (*function) (uint8_t)) {
	setRAMBank(bankNr);
	
	switch(gameCartHead.usesController) {
		case MBC2:
			readDataFromTo(0xA000, 0xA1FF, function);
			
			break;
		default:
			readDataFromTo(0xA000, 0xBFFF, function);
			break;		
	}

}

void readRAMBanks(uint8_t bankStart, uint8_t bankEnd, void (*function) (uint8_t)) {
	uint16_t i=0;
	for(i=bankStart;i<=bankEnd;i++) {
		readRAMBank(i, function);
	}
}

void readRAM(void (*function) (uint8_t)) {
	readGameCartHeader();
	uint8_t bankCount=getRAMBankCounter();
	setRAMEnabled(true);
	readRAMBanks(0,bankCount,function);
	setRAMEnabled(false);
}


void writeRAM(uint8_t (*function) (void)) {
	readGameCartHeader();
	uint8_t bankCount=getRAMBankCounter();
	uint16_t addressCounter=0;
	uint8_t bankTemp=0;
	uint8_t currByte=0;
	
	setRAMEnabled(true);
	for(bankTemp=0;bankTemp<=bankCount;bankTemp++) {
		setRAMBank(bankTemp);
		
		for(addressCounter=0xA000;addressCounter<=0xBFFF;addressCounter++) {
			currByte=function();
			writeDataByte(addressCounter, currByte);
		}
		
	}
	
	setRAMEnabled(false);
}