/*
 * GBCReader.h
 *
 * Created: 28.12.2015 15:52:26
 *  Author: Steven
 */ 


#ifndef GBREADER_H_
	#define GBREADER_H_
	#include "../Configuration.h"
	#include <avr/io.h>
	#include <stdlib.h>
	#include <avr/pgmspace.h>
	#include <util/delay.h>
	
	//Header Addresses and Sizes
		#define logoAddress 0x0104
		#define logoSize 48
		#define titleAddress 0x0134
		#define titleSize 16
		#define manufacturerCodeAddress 0x013F
		#define manufacturerCodeSize 4
		#define cgbFlagAddress 0x0143
		#define newLicenseeCodeAddress 0x0144
		#define newLicenseeCodeSize 2
		#define sgbFlagAddress 0x0146
		#define cartTypeAddress 0x0147
		#define romSizeAddress 0x0148
		#define ramSizeAddress 0x0149
		#define destinationAddress 0x014A
		#define oldLicenseeCodeAddress 0x014B
		#define maskRomNumberAddress 0x014C
		#define headerChecksumAddress 0x014D
		#define globalChecksumAddress 0x014E
		#define globalChecksumSize 2
	
		//Various
		#define headerChecksumStartAddress 0x0134
		#define headerChecksumEndAddress 0x014C
				
	//Results
		//CGB Flag
		#define cgbFlagOldGameboy 0x80
		#define cgbFlagCGBOnly 0xC0

		//SGB Flag
		#define sgbFlagNoSGB 0x00
		#define sgbFlagSGBSupport 0x03

		//Cartridge Types
		#define cartTypeROMOnly 0x00
		#define cartTypeMBC1 0x01
		#define cartTypeMBC1RAM 0x02
		#define cartTypeMBC1RAMBattery 0x03
		#define cartTypeMBC2 0x05
		#define cartTypeMBC2Battery 0x06
		#define cartTypeROMRAM 0x08
		#define cartTypeROMRAMBattery 0x09
		#define cartTypeMMM01 0x0B
		#define cartTypeMMM01RAM 0x0C
		#define cartTypeMMM01RAMBattery 0x0D
		#define cartTypeMBC3TimerBattery 0x0F
		#define cartTypeMBC3TimerRAMBattery 0x10
		#define cartTypeMBC3 0x11
		#define cartTypeMBC3RAM 0x12
		#define cartTypeMBC3RAMBattery 0x13
		#define cartTypeMBC4 0x15
		#define cartTypeMBC4RAM 0x16
		#define cartTypeMBC4RAMBattery 0x17
		#define cartTypeMBC5 0x19
		#define cartTypeMBC5RAM 0x1A
		#define cartTypeMBC5RAMBattery 0x1B
		#define cartTypeMBC5Rumble 0x1C
		#define cartTypeMBC5RumbleRAM 0x1D
		#define cartTypeMBC5RumbleRAMBattery 0x1E
		#define cartTypePocketCamera 0xFC
		#define cartTypeBandaiTama5 0xFD
		#define cartTypeHuC3 0xFE
		#define cartTypeHuC1RamBattery 0xFF

		//ROM Sizes
		#define romSize32K 0x00
		#define romSize64K 0x01
		#define romSize128K 0x02
		#define romSize256K 0x03
		#define romSize512K 0x04
		#define romSize1M 0x05
		#define romSize2M 0x06
		#define romSize4M 0x07
		#define romSize1_1M 0x52
		#define romSize1_2M 0x53
		#define romSize1_5M 0x54

		//RAM Sizes
		#define ramSizeNone 0x00
		#define ramSize2K 0x01
		#define ramSize8K 0x02
		#define ramSize32K 0x03
		#define ramSize128K 0x04
		#define ramSize64K 0x05

		//Destination Codes
		#define destinationJapanese 0x00
		#define destinationNonJapanese 0x01

		//If the oldLicensseCode is 0x33 use the newer LicenseeCode
		#define oldLicenseeCodeUseNew 0x33
	
	//Enums
		#define ROM 0
		#define MBC1 1
		#define MBC2 2
		#define MBC3 3
		#define MBC4 4
		#define MBC5 5
		#define MBC6 6
		#define MMM01 7
	
	//Typedefinitions and data structures
		typedef uint8_t bool;
		#define true 1
		#define false 0
		
		typedef struct {
			uint8_t *logo;
			uint8_t *title;
			uint8_t *manufacturerCode;
			uint8_t cgbFlag;
			uint8_t *newLicenseeCode;
			uint8_t sgbFlag;
			
			uint8_t usesController; //0 is no MBC chip
			bool hasRAM;
			bool hasBattery;
			bool hasRTC;
			bool isUnsupported;
			
			uint8_t romSize;
			uint8_t ramSize;
			uint8_t destination;
			uint8_t oldLicenseeCode;
			uint8_t maskRomNumber;
			uint8_t headerChecksum;
			uint8_t *globalChecksum;
		}CartHeader;		

	CartHeader gameCartHead;
	
	//Function Prototypes	
		void initROM();
		void readGameCartHeader();
		void writeAddress(uint16_t address);
		void writeDataByte(uint16_t address, uint8_t byte);
		uint8_t readDataByte(uint16_t address);
		uint8_t* readDataBytesToRAM(uint16_t startAddress, uint8_t size);
		void setROMBank(uint8_t bank);
		CartHeader* getCartHeader();
		uint8_t verifyLogo();
		void readROM(void (*function) (uint8_t));
		uint8_t checkHeaderChecksum();
#endif /* GBCREADER_H_ */