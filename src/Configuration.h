/*
 * Configuration.h
 *
 * Created: 28.12.2015 15:42:10
 *  Author: Steven
 */ 


#ifndef CONFIGURATION_H_ //Do not Change
	#define CONFIGURATION_H_ //Do not Change
	
	//Clock Settings
	#define F_CPU 16000000
	#define BAUD 1000000

	//In/Output Configuration
	//Addresses
	#define A0ToA7Port PORTA
	#define A0ToA7DDR DDRA
	#define A8ToA15Port PORTB
	#define A8ToA15DDR DDRB

	//DataInput
	#define DataPort PORTC
	#define DataPin PINC
	#define DataDDR DDRC

	//Control Outputs
	#define CTRLPort PORTD
	#define CTRLDDR DDRD

	//Control Pins
	#define CLK PD4
	#define READ PD5
	#define WRITE PD6
	#define CS PD3
	#define RESET PD2

#endif /* CONFIGURATION_H_ */ //Do not Change