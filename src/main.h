/*
 * main.h
 *
 * Created: 28.12.2015 15:56:39
 *  Author: Steven
 */ 


#ifndef MAIN_H_
	#define MAIN_H_
	#include "CartReader/GBReader.h"
	#include "CartReader/RAMReader.h"
	#include "CartReader/ROMReader.h"
	#include "Drivers/UARTDriver.h"	
	#include <avr/io.h>
	#include <stdlib.h>
	#include <avr/pgmspace.h>

	//Functions
	uint8_t waitforRequiredChar(uint8_t start, uint8_t end);
	void showMainMenu();
	void showHeaderMenu();
	int main();
#endif /* MAIN_H_ */